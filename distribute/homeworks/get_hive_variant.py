#! /usr/bin/env python3

import string
import sys

tasks = list(range(1, 4))

if __name__ == '__main__':
    result = 0
    for symbol in sys.argv[1].strip():
        for num, elem in enumerate(string.ascii_lowercase):
            if symbol == elem:
                result += num
                break
    print(tasks[result % len(tasks)])
