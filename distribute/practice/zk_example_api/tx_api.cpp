#include <argagg/argagg.hpp>
#include <iostream>
#include <cstdlib>
#include <future>
#include <zookeeper.h>
#include <cassert>
#include <chrono>

/* Для начала познакомимся с синхронным API
 * Научимся создавать ноду
 * Потом научимся создавать в ней эфемерного ребёнка с данными
 * и прочитаем данные в нём
 */


void run(const std::string &cluster, const std::string &root) {

  // Инициализируем коннект к кластеру
  // zoo_set_debug_level(ZOO_LOG_LEVEL_DEBUG);
  auto zh = zookeeper_init(cluster.data(), nullptr, 10000, nullptr, nullptr, 0);

  char buffer[3000];
  int buflen = 3000;

  //
  // Создаём корень для работы
  //
  auto rc = zoo_create(
      zh,
      root.data(),
      nullptr,  // данные для записи
      0,      // размер данных
      &ZOO_OPEN_ACL_UNSAFE,
      ZOO_PERSISTENT,  //  ZOO_EPHEMERAL, ZOO_PERSISTENT_SEQUENTIAL & etc
      buffer,  // Сюда попадёт название созданного узла. оно может отличаться от root из-за флага SEQUENTIAL
      buflen  // размер буфера
  );
  std::cerr << "=== Create root result: " << zerror(rc) << "\n";
  assert((rc == ZOK) || (rc == ZNODEEXISTS));
  std::cerr << "Created root\n";

  /*
   * Транзакционно создаём 2 узла
   */

  zoo_op_t ops[2];
  zoo_op_result_t ops_result[2];
  std::string rbufs[2];
  rbufs[0].resize(3000);
  rbufs[1].resize(3000);

  std::string path_template = root + "/item-";
  zoo_create_op_init(
      &(ops[0]),
      path_template.data(),
      nullptr,
      0,
      &ZOO_OPEN_ACL_UNSAFE,
      ZOO_EPHEMERAL_SEQUENTIAL,
      (char*) rbufs[0].data(),
      rbufs[0].capacity()
    );
  zoo_create_op_init(
      &(ops[1]),
      path_template.data(),
      nullptr,
      0,
      &ZOO_OPEN_ACL_UNSAFE,
      ZOO_EPHEMERAL_SEQUENTIAL,
      (char*) rbufs[1].data(),
      rbufs[1].capacity()
  );

  auto rv = zoo_multi(
      zh,
      2,
      ops,
      ops_result
    );
  assert(rv == ZOK);

  std::cerr << "Created znode. Path: '" << rbufs[0].data() << "'\n";
  std::cerr << "Created znode. Path: '" << rbufs[1].data() << "'\n";

  std::string first = rbufs[0];
  std::string second = rbufs[1];

  /*
   * Теперь транзакционно удалим первую и создадим третью
   */


  std::cerr << "Deleting ....\n";
  zoo_delete_op_init(
      &(ops[0]),
      rbufs[0].data(),
      -1
  );
  assert(rv == ZOK);
  // ops[1] не меняется

  rv = zoo_multi(
      zh,
      2,
      ops,
      ops_result
  );
  assert(rv == ZOK);
  std::cerr << "Delete result " << zerror(ops_result[0].err) << " " << ops_result->value << "\n";
  std::cerr << "Create result " << zerror(ops_result[1].err) << " " << ops_result->value << "\n";


  /*
   * Теперь транзакционно и повторно удалим первую и создадим четвертую
   */

  zoo_create_op_init(
      &(ops[0]),
      path_template.data(),
      nullptr,
      0,
      &ZOO_OPEN_ACL_UNSAFE,
      ZOO_EPHEMERAL_SEQUENTIAL,
      (char*) rbufs[0].data(),
      rbufs[0].capacity()
  );

  zoo_delete_op_init(
      &(ops[1]),
      first.data(),
      -1
  );
  std::cerr << "Deleting #2 ....\n";
  // операции настроены уже
  rv = zoo_multi(
      zh,
      2,
      ops,
      ops_result
  );
  std::cerr << "TX ERR: " << zerror(rv) << "\n";

  std::cerr << "Create result:  " << zerror(ops_result[0].err) << "\n";
  std::cerr << "Delete result:  " << zerror(ops_result[1].err) << "\n";
  // assert(rv == ZOK);

  // NOTE: деструкторов нет, сессию нужно закрывать самостоятельно
  // Если этого не сделать, то ваши эфемерные ноды будут прохухать ещё долго (долго отпускаются локи)
  std::this_thread::sleep_for(std::chrono::seconds{2});
  zookeeper_close(zh);
}

int main(int argc, char **argv) {
  argagg::parser argument_parser{
      {
          {
              "cluster", {"-c", "--cluster"},
              "Zookeeper cluster connection string", 1
          },
          {
              "root", {"-r", "--root"},
              "Zookeeper root", 1
          },

      }
  };

  argagg::parser_results args;
  try {
    args = argument_parser.parse(argc, argv);
  } catch (const std::exception &e) {
    argagg::fmt_ostream fmt(std::cerr);
    fmt << "Bad usage!" << argument_parser << std::endl
        << "Encountered exception while parsing arguments: " << e.what()
        << std::endl;
    return EXIT_FAILURE;
  }

  if (!args["cluster"] || !args["root"]) {
    std::cout << "Bad usage! --cluster and --root required." << std::endl;
    return EXIT_FAILURE;
  }

  std::string cluster = args["cluster"];
  std::string root = args["root"];

  run(cluster, root);

  return EXIT_SUCCESS;
}


