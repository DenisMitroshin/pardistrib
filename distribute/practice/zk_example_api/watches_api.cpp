#include <argagg/argagg.hpp>
#include <iostream>
#include <cstdlib>
#include <future>
#include <zookeeper.h>
#include <cassert>
#include <chrono>

/*
 * Задача: Напишите класс, который будет
 * подписываться на узел ZK и кешировать его состояние и версию.
 */



static const char *state2String(int state);
static const char *watcherEvent2String(int ev);


/*
 * Класс сохраняет и автообновляет копию данных узла в ZK.
 * Будьте осторожны, никто не обещает нам, что мы увидим все апдейты.
 */
struct NodeCache {
  std::string persisted_state;

  static void WatcherProxy(zhandle_t *zh, int type, int state, const char *path, void *ctx) {
    NodeCache *This = (NodeCache *) ctx;
    This->WatcherReal(zh, type, state, path, nullptr);
  }

  // Сигнатура watcher_fn из ZK API
  void WatcherReal(zhandle_t *zh, int type, int state, const char *path, void *ctx) {
    /*
     * Обратите внимание на наличие zhandle_t* в аргументах.
     *
     * Нам может прилететь эвент связанный с изменением данных (ZOO_CHANGED_EVENT)
     *
     * Или может прилететь, например state=ZOO_EXPIRED_SESSION_STATE. В этом случае это будет
     * означать, что сессия, в рамках которой мы ждали watch -- протухла и можно больше не ждать.
     */

    std::cerr << "Got Event! Type: " << watcherEvent2String(type) << ", " \
 << "State: " << state2String(state) << ", Path: " << path << "\n";
    if (type == ZOO_CHANGED_EVENT) {
      auto rc = zoo_awget(
          zh,
          path,
          NodeCache::WatcherProxy,
          this,
          NodeCache::DataCompletionProxy,
          this
      );
      assert(rc == ZOK);
    } else {
      assert(false);
    }

    /*
     * Раскомментируйте sleep ниже и посмотрите, что будет
     */
    // std::this_thread::sleep_for(std::chrono::seconds{6});

    /* Zokeeper обещает, что будет запускать коллбеки в порядке событий. Сделано это очень просто --
     * порядок коллбеков задаёт зукипер, а их запуск в одном потоке в порядке очереди.
     * Поэтому не стоит блокироваться в коллбеках
     *
     * zoo_awget(watch=1) мы выше узже вызвали, т.е. запрос на выставление watch отправлен, а получим мы его только после
     * выхода из этой функции, причём версия znode будет весьма старой.
     */


  }

  static void DataCompletionProxy(int rc, const char *value, int value_len,
                                  const struct Stat *stat, const void *data) {
    auto This = (NodeCache *) data;
    std::string result;
    result.assign(value, value_len);
    This->DataCompletionReal(rc, value, stat);
  }

  // Сигнатура watcher_fn из ZK API
  void DataCompletionReal(int rc, const std::string &value, const Stat *stat) {
    assert(rc == ZOK);
    std::cerr << "Updated node cache. Version: " << stat->version << ", Payload: '" << value << "'\n";
    // Сохраняем новый стейт и версию
    persisted_state = value;
  }

};


void run(const std::string &cluster, const std::string &root) {
  auto zh = zookeeper_init(cluster.data(), nullptr, 10000, nullptr, nullptr, 0);
  std::string buffer;
  buffer.resize(3000);

  //
  // Создаём узел, на который будем подписываться
  //
  auto rc = zoo_create(
      zh,
      root.data(),
      nullptr,
      0,
      &ZOO_OPEN_ACL_UNSAFE,
      ZOO_PERSISTENT,
      (char *) buffer.data(),
      buffer.capacity()
  );
  assert((rc == ZOK) || (rc == ZNODEEXISTS));

  // Делаем zhandle, которым будет пользоваться node_cache
  auto zh_node_cache = zookeeper_init(cluster.data(), nullptr, 10000, nullptr, nullptr, 0);

  NodeCache node_cache{};

  // Эмулируем апдейт, чтобы node_cache выставил вотч и инициализировался
  NodeCache::WatcherProxy(zh_node_cache, ZOO_CHANGED_EVENT, ZOO_CONNECTED_STATE, root.data(), &node_cache);

  for (int i = 0; i < 10; ++i) {
    std::string buf = "payload N" + std::to_string(i);
    rc = zoo_set(zh, root.data(), buf.data(), buf.size(), -1);
    assert(rc == ZOK);
    std::cerr << "Set new payload. Iteration: " << i << "\n";
    std::this_thread::sleep_for(std::chrono::seconds{1});
  }

  // даём добежать коллбекам, если не добегут -- упадём.
  std::this_thread::sleep_for(std::chrono::seconds{5});
  zookeeper_close(zh);
  zookeeper_close(zh_node_cache);

}

int main(int argc, char **argv) {
  argagg::parser argument_parser{
      {
          {
              "cluster", {"-c", "--cluster"},
              "Zookeeper cluster connection string", 1
          },
          {
              "root", {"-r", "--root"},
              "Zookeeper root", 1
          },

      }
  };

  argagg::parser_results args;
  try {
    args = argument_parser.parse(argc, argv);
  } catch (const std::exception &e) {
    argagg::fmt_ostream fmt(std::cerr);
    fmt << "Bad usage!" << argument_parser << std::endl
        << "Encountered exception while parsing arguments: " << e.what()
        << std::endl;
    return EXIT_FAILURE;
  }

  if (!args["cluster"] || !args["root"]) {
    std::cout << "Bad usage! --cluster and --root required." << std::endl;
    return EXIT_FAILURE;
  }

  std::string cluster = args["cluster"];
  std::string root = args["root"];

  run(cluster, root);

  return EXIT_SUCCESS;
}



// Копипаста из ZK


/* zookeeper event type constants */
#define CREATED_EVENT_DEF 1
#define DELETED_EVENT_DEF 2
#define CHANGED_EVENT_DEF 3
#define CHILD_EVENT_DEF 4
#define SESSION_EVENT_DEF -1
#define NOTWATCHING_EVENT_DEF -2

static const char *watcherEvent2String(int ev) {
  switch (ev) {
    case 0:return "ZOO_ERROR_EVENT";
    case CREATED_EVENT_DEF:return "ZOO_CREATED_EVENT";
    case DELETED_EVENT_DEF:return "ZOO_DELETED_EVENT";
    case CHANGED_EVENT_DEF:return "ZOO_CHANGED_EVENT";
    case CHILD_EVENT_DEF:return "ZOO_CHILD_EVENT";
    case SESSION_EVENT_DEF:return "ZOO_SESSION_EVENT";
    case NOTWATCHING_EVENT_DEF:return "ZOO_NOTWATCHING_EVENT";
    default:return "INVALID_EVENT";
  }
}

#define EXPIRED_SESSION_STATE_DEF -112
#define AUTH_FAILED_STATE_DEF -113
#define CONNECTING_STATE_DEF 1
#define ASSOCIATING_STATE_DEF 2
#define CONNECTED_STATE_DEF 3
#define READONLY_STATE_DEF 5
#define NOTCONNECTED_STATE_DEF 999

static const char *state2String(int state) {
  switch (state) {
    case 0:return "ZOO_CLOSED_STATE";
    case CONNECTING_STATE_DEF:return "ZOO_CONNECTING_STATE";
    case ASSOCIATING_STATE_DEF:return "ZOO_ASSOCIATING_STATE";
    case CONNECTED_STATE_DEF:return "ZOO_CONNECTED_STATE";
    case READONLY_STATE_DEF:return "ZOO_READONLY_STATE";
    case EXPIRED_SESSION_STATE_DEF:return "ZOO_EXPIRED_SESSION_STATE";
    case AUTH_FAILED_STATE_DEF:return "ZOO_AUTH_FAILED_STATE";
    default:return "INVALID_STATE";
  }
}
