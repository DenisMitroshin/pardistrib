cmake_minimum_required(VERSION 3.0)
project(boo)


set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)


set(SRC ${CMAKE_CURRENT_LIST_DIR}/contrib/zookeeper-client-c)
set(DST ${CMAKE_BINARY_DIR}/contrib/zookeeper)
set(THREADED ON)
add_subdirectory(${SRC})

set(ROOT ${CMAKE_CURRENT_LIST_DIR}/contrib/argagg)
add_library(argagg INTERFACE)
target_include_directories(argagg INTERFACE
        $<BUILD_INTERFACE:${ROOT}>
        $<INSTALL_INTERFACE:include>)

add_definitions(-DTHREADED)
add_executable(sync_api sync_api.cpp)
target_link_libraries(sync_api argagg)
target_link_libraries(sync_api zookeeper)

add_executable(async_api async_api.cpp)
target_link_libraries(async_api argagg)
target_link_libraries(async_api zookeeper)

add_executable(watches_api watches_api.cpp)
target_link_libraries(watches_api argagg)
target_link_libraries(watches_api zookeeper)

add_executable(tx_api tx_api.cpp)
target_link_libraries(tx_api argagg)
target_link_libraries(tx_api zookeeper)

