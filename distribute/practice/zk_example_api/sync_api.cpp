#include <argagg/argagg.hpp>
#include <iostream>
#include <cstdlib>
#include <zookeeper.h>
#include <cassert>

/* Для начала познакомимся с синхронным API
 * Научимся создавать ноду
 * Потом научимся создавать в ней эфемерного ребёнка с данными
 * и прочитаем данные в нём
 */


void run(const std::string &cluster, const std::string &root) {

  // Инициализируем коннект к кластеру
  auto zh = zookeeper_init(cluster.data(), nullptr, 10000, nullptr, nullptr, 0);

  char buffer[3000];
  int buflen = 3000;

  //
  // Создаём корень для работы
  //
  auto rc = zoo_create(
      zh,
      root.data(),
      nullptr,  // данные для записи
      0,      // размер данных
      &ZOO_OPEN_ACL_UNSAFE,
      ZOO_PERSISTENT,  //  ZOO_EPHEMERAL, ZOO_PERSISTENT_SEQUENTIAL & etc
      buffer,  // Сюда попадёт название созданного узла. оно может отличаться от root из-за флага SEQUENTIAL
      buflen  // размер буфера
  );
  std::cerr << "=== Create root result: " << zerror(rc) << "\n";
  assert((rc == ZOK) || (rc == ZNODEEXISTS));
  std::cerr << "Created root\n";

  /*
   * Создаём эфемерную ноду с данными
   */

  std::string payload{"boo bar baz"};
  rc = zoo_create(
      zh,
      (root + "/target-").data(),
      payload.data(),  // Данные и размер данных
      payload.size(),
      &ZOO_OPEN_ACL_UNSAFE,
      ZOO_EPHEMERAL_SEQUENTIAL,
      buffer,
      buflen
  );
  std::cerr << "=== Create EPHEMERAL child with payload result: " << zerror(rc) << "\n";
  assert(rc == ZOK);

  std::string strbuf{buffer};

  std::cerr << "Created Child: Data: '" << strbuf << "\n";

  std::string target = strbuf;

  //
  // Пытаемся вычитать данные
  //
  Stat stat{};  // ZStat из ZK_API
  int result_length = buflen;
  rc = zoo_get(
      zh,
      target.data(),
      0,
      buffer,
      &result_length,  // Передаём размер буфера, длинна результата будет тут
      &stat
      );

  std::cerr << "=== Get result: " << zerror(rc) << "\n";
  assert(rc == ZOK);

  // Пути null-terminated, данные -- нет. Поэтому использовать strlen тут плохая идея.
  // К счастью нам возвращают длинну результата в result_length
  std::cerr << "Uncorrected result. " << \
               "Data: '" << buffer << "', " \
               "Result size: " << result_length << ", " \
               "Strlen: " << strlen(buffer) << "\n";


  std::string str_get_payload;
  str_get_payload.assign(buffer, result_length);
  std::cerr << "Corrected result. " << \
               "Data: '" << str_get_payload << "'\n";  // OK!

  std::cerr << "Zstat: Version: " << stat.version << ", " \
            << "CVersion: " << stat.cversion << ", " \
            << "CZxid: " << stat.czxid << ", "\
            << "MZxid: " << stat.mzxid << ", "\
            << "EphemeralOwner: " << stat.ephemeralOwner << ", "\
            << "DataLength: " << stat.dataLength << ", "\
            << "MTime: " << stat.mtime << "\n";

  //
  // Закрываем соединение, эфемерная нода удаляется.
  //
  // NOTE: деструкторов нет, сессию нужно закрывать самостоятельно
  // Если этого не сделать, то ваши эфемерные ноды будут прохухать ещё долго (долго отпускаются локи)
  zookeeper_close(zh);
}

int main(int argc, char **argv) {
  argagg::parser argument_parser{
      {
          {
              "cluster", {"-c", "--cluster"},
              "Zookeeper cluster connection string", 1
          },
          {
              "root", {"-r", "--root"},
              "Zookeeper root", 1
          },

      }
  };

  argagg::parser_results args;
  try {
    args = argument_parser.parse(argc, argv);
  } catch (const std::exception &e) {
    argagg::fmt_ostream fmt(std::cerr);
    fmt << "Bad usage!" << argument_parser << std::endl
        << "Encountered exception while parsing arguments: " << e.what()
        << std::endl;
    return EXIT_FAILURE;
  }

  if (!args["cluster"] || !args["root"]) {
    std::cout << "Bad usage! --cluster and --root required." << std::endl;
    return EXIT_FAILURE;
  }

  std::string cluster = args["cluster"];
  std::string root = args["root"];

  run(cluster, root);

  return EXIT_SUCCESS;
}

