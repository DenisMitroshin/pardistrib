#include <argagg/argagg.hpp>
#include <iostream>
#include <cstdlib>
#include <future>
#include <zookeeper.h>
#include <cassert>

/*
 * Пример асинхронного API
 *
 * Создадим 100 Sequential nodes где-то в каталоге, а потом асинхронно прочитаем их
 *
 * Для обработки результата сделаем специальный класс Aggregator и выставим его как коллбек метода zoo_aget(
 */


struct Aggregator {
  std::mutex lock_;
  std::condition_variable wait_cv_;
  int result_;
  int need_results_;
  int got_results_;

  explicit Aggregator(int result_count) : result_{0}, need_results_{result_count}, got_results_{0} {
    // noop
  }

  void Callback(int rc, const char *value, int value_len, const struct Stat *stat, const void *data) {

    // Note: коллбеки запускаются в отдельном потоке, порождаемом ZK клиентом. Поэтому нужно думать от thread safety
    // Кто не помнит курс tpcc -- нам достаточно mutex и conditional_variable
    if (rc != ZOK) {
      std::cerr << "Call failed " << zerror(rc) << "\n";
      assert(false);
      // Внезапно в коллбеке сложно поянть какой узел мы читали. Можно передать информацию через data.
      // Например, вместо &Aggregator передать пару std::pair{path, &Aggregator}
      // Или использовать разные функции под специализированные ноды (обычно так и делают)
    }
    std::cerr << "Processing async result. Error: " << zerror(rc) << "\n";
    std::string buf;
    buf.assign(value, value_len);

    std::lock_guard<std::mutex> lg{lock_};
    result_ += std::stoi(buf);
    got_results_ += 1;
    if (got_results_ == need_results_) {
      wait_cv_.notify_all();
    }
  }

  int Get() {
    std::unique_lock<std::mutex> lg{lock_};
    while(true) {
      if (need_results_ == got_results_) {
        break;
      } else {
        wait_cv_.wait(lg);
      }
    }
    // lock всё ещё держим
    return result_;
  }
};

//
// Callback с сигнатурой data_completion_t из ZK API
//

void AggregatorProxy(int rc, const char *value, int value_len, const struct Stat *stat, const void *data) {
  auto TheAggregator = (Aggregator*) data;
  TheAggregator->Callback(rc, value, value_len, stat, data);
}

void run(const std::string &cluster, const std::string &root) {
  auto zh = zookeeper_init(cluster.data(), nullptr, 10000, nullptr, nullptr, 0);

  std::string buffer;
  buffer.resize(3000);

  //
  // Создаём корень для работы, нет смысла делать это асинхронно
  //
  auto rc = zoo_create(
      zh,
      root.data(),
      nullptr,
      0,
      &ZOO_OPEN_ACL_UNSAFE,
      ZOO_PERSISTENT,
      (char *) buffer.data(),
      buffer.capacity()
  );
  std::cerr << "=== Create root result: " << zerror(rc) << "\n";
  assert((rc == ZOK) || (rc == ZNODEEXISTS));

  //
  // Создаём 10 последовательных нод синхронно
  //
  for (int i = 0; i < 10; ++i) {
    std::string payload{std::to_string(i)};
    rc = zoo_create(
        zh,
        (root + "/task-").data(),
        payload.data(),
        payload.size(),
        &ZOO_OPEN_ACL_UNSAFE,
        ZOO_EPHEMERAL_SEQUENTIAL,
        (char *) buffer.data(),
        buffer.capacity()
    );
    assert(rc == ZOK);
    buffer.resize(strlen(buffer.data()));
    std::cerr << "Created root: Path: '" << buffer.data() << "\n";
  }

  //
  // Старт асинхронной части
  //
  // Вычитываем список нод в узле, для этого нам понадобится странная структура из ZK API String_vector
  auto* stringVector = new String_vector{};
  rc = zoo_get_children(zh, root.data(), 0, stringVector);
  std::cerr << zerror(rc) << "\n";
  assert(rc == ZOK);

  Aggregator aggr{stringVector->count};
  for (int i = 0; i < stringVector->count; ++i) {
    std::string buf = root + "/" + stringVector->data[i];
    // Собственно отправка запросов. API синхронно сериализует запрос и кладёт в очередь на отправку
    rc = zoo_aget(zh, buf.data(), 0, &AggregatorProxy, &aggr);
    //
    std::cerr << "Async get. Path: "  << buf << " Result: " << zerror(rc) << "\n";
    assert(rc == ZOK);
  }
  deallocate_String_vector(stringVector);

  int result = aggr.Get();
  std::cerr << "Sum result: " << result << "\n";
  // NOTE: деструкторов нет, сессию нужно закрывать самостоятельно
  zookeeper_close(zh);
}

int main(int argc, char **argv) {
  argagg::parser argument_parser{
      {
          {
              "cluster", {"-c", "--cluster"},
              "Zookeeper cluster connection string", 1
          },
          {
              "root", {"-r", "--root"},
              "Zookeeper root", 1
          },

      }
  };

  argagg::parser_results args;
  try {
    args = argument_parser.parse(argc, argv);
  } catch (const std::exception &e) {
    argagg::fmt_ostream fmt(std::cerr);
    fmt << "Bad usage!" << argument_parser << std::endl
        << "Encountered exception while parsing arguments: " << e.what()
        << std::endl;
    return EXIT_FAILURE;
  }

  if (!args["cluster"] || !args["root"]) {
    std::cout << "Bad usage! --cluster and --root required." << std::endl;
    return EXIT_FAILURE;
  }

  std::string cluster = args["cluster"];
  std::string root = args["root"];

  run(cluster, root);

  return EXIT_SUCCESS;
}


